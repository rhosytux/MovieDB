package com.elangtech.moviedb.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elangtech.moviedb.R;
import com.elangtech.moviedb.model.TopRatedMoviesData;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ElangTECH on 9/3/18.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> {

    private List<TopRatedMoviesData> topRatedMoviesDataList;
    private Context mContext;
    private static final String BASE_IMG_URL = "https://image.tmdb.org/t/p/w500";

    public HomeAdapter(List<TopRatedMoviesData> topRatedMoviesDataList, Context mContext) {
        this.topRatedMoviesDataList = topRatedMoviesDataList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_movie, parent, false);
        return new HomeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {

        TopRatedMoviesData moviesData = topRatedMoviesDataList.get(holder.getAdapterPosition());
        holder.tvTitle.setText(moviesData.getTitle());
        holder.tvGenre.setText(moviesData.getReleaseDate());
        String posterUri = moviesData.getPosterPath();
        Picasso.with(mContext).load(BASE_IMG_URL+posterUri).into(holder.imgPoster);


    }

    @Override
    public int getItemCount() {
        return topRatedMoviesDataList.size();
    }

    public class HomeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_poster)
        ImageView imgPoster;

        @BindView(R.id.tv_movie_title)
        TextView tvTitle;

        @BindView(R.id.tv_movie_genre)
        TextView tvGenre;

        public HomeHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
