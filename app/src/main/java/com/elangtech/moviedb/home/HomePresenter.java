package com.elangtech.moviedb.home;

import android.content.Context;
import android.view.View;

import com.elangtech.moviedb.model.TopRatedMovies;
import com.elangtech.moviedb.networking.ApiBuilder;
import com.elangtech.moviedb.networking.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ElangTECH on 9/3/18.
 */
public class HomePresenter {

    private HomeView homeView;
    private Context mContext;

    public HomePresenter(HomeView homeView, Context mContext) {
        this.homeView = homeView;
        this.mContext = mContext;
    }

    public void getTopMovieList(){

        ApiService service = ApiBuilder.call();
        service.getTopRatedMovies("903fbbf998820b5569f8d4b9a6517d52", "en-US", 1).enqueue(new Callback<TopRatedMovies>() {
            @Override
            public void onResponse(Call<TopRatedMovies> call, Response<TopRatedMovies> response) {
                homeView.setVisibilityProgressBar(View.GONE);
                homeView.getTopRatedMoviesSucces(response.body().getTopRatedMoviesData());
            }

            @Override
            public void onFailure(Call<TopRatedMovies> call, Throwable t) {
                homeView.setVisibilityProgressBar(View.GONE);
                homeView.showToast("Failed to load data");

            }
        });
    }
}
