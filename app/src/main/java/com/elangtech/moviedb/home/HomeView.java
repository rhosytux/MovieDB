package com.elangtech.moviedb.home;

import com.elangtech.moviedb.model.TopRatedMovies;
import com.elangtech.moviedb.model.TopRatedMoviesData;

import java.util.List;

/**
 * Created by ElangTECH on 9/3/18.
 */

public interface HomeView {

    void setVisibilityProgressBar(int visibility);

    void getTopRatedMoviesSucces(List<TopRatedMoviesData> topRatedMoviesList);

    void showToast(String msg);
}
