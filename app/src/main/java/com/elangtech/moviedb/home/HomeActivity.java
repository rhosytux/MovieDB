package com.elangtech.moviedb.home;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.elangtech.moviedb.R;
import com.elangtech.moviedb.model.TopRatedMoviesData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements HomeView{

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.rv_topmovies)
    RecyclerView rvTopMovies;

    private HomePresenter presenter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        initializePresenter();
    }

    private void initializePresenter() {
        mContext = this;
        presenter = new HomePresenter(this, mContext);
        presenter.getTopMovieList();
    }

    @Override
    public void setVisibilityProgressBar(int visibility) {

        switch (visibility){
            case View.GONE:
                progressBar.setVisibility(visibility);
                rvTopMovies.setVisibility(View.VISIBLE);
                break;
            case View.VISIBLE:
                progressBar.setVisibility(visibility);
                rvTopMovies.setVisibility(View.GONE);
        }

    }

    @Override
    public void getTopRatedMoviesSucces(List<TopRatedMoviesData> topRatedMoviesList) {

        HomeAdapter adapter = new HomeAdapter(topRatedMoviesList, mContext);

        rvTopMovies.setLayoutManager(new GridLayoutManager(this, 2));
        rvTopMovies.setAdapter(adapter);

    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }
}
