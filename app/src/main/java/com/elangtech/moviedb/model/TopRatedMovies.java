package com.elangtech.moviedb.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ElangTECH on 9/3/18.
 */
public class TopRatedMovies {

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total_results")
    @Expose
    private Integer totalResults;
    @SerializedName("total_pages")
    @Expose
    private Integer totalPages;
    @SerializedName("results")
    @Expose
    private List<TopRatedMoviesData> topRatedMoviesData;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<TopRatedMoviesData> getTopRatedMoviesData() {
        return topRatedMoviesData;
    }

    public void setTopRatedMoviesData(List<TopRatedMoviesData> topRatedMoviesData) {
        this.topRatedMoviesData = topRatedMoviesData;
    }
}
