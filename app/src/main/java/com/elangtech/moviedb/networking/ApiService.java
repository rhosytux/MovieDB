package com.elangtech.moviedb.networking;

import com.elangtech.moviedb.model.TopRatedMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("movie/top_rated")
    Call<TopRatedMovies> getTopRatedMovies(@Query("api_key") String api_key,
                                           @Query("language") String language,
                                           @Query("page") int page);

}
